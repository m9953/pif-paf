#include<iostream>
#include<chrono>
#include <windows.h>
#include <thread>


#include<SFML/Window.hpp>
#include<SFML/System.hpp>
#include<SFML/Graphics.hpp>

#define _USE_MATH_DEFINES 
#include <math.h>

#include "Common.h"
#include "Player.h"
#include "Enemy.h"
#include "Bullet.h"
  

using namespace std;

std::chrono::time_point<std::chrono::steady_clock> prev_time;

//int winx = GetSystemMetrics(SM_CXFULLSCREEN);
//int winy = GetSystemMetrics(SM_CYFULLSCREEN);

const int H = 27;
const int W = 70;

vector<float> offset{0, 0};

string scene[H] = {
"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW",
"W                                                    W               W",
"W                                                    W               W",
"W                                                    W               W",
"W                                  W                 W     W         W",
"W                      W           W                 W               W",
"W                      W           WWW               W               W",
"W                      W           WWW               W               W",
"WWWWWWWWWWW            W             W                               W",
"W                      W             W                               W",
"W                      W             W                               W",
"WWWWWWWWWWW            W             W                               W",
"W                      W             WWWWW                           W",
"W                      W             WWWWW                           W",
"W                      W             WWWWW                           W",
"W                      W                 WW                          W",
"W             W                           W                          W",
"W             W                                      WWWWWWWWWWWWWWWWW",
"W             W                                                      W",
"W             W                                                      W",
"W             W                                                      W",
"W             W                                                      W",
"W             W                                                      W",
"W             W                                                      W",
"W             W                                                      W",
"W             W                                                      W",
"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
};

int WINDOWX = 1000, WINDOWY = 800;
bool onPause = false;
sf::RenderWindow window(sf::VideoMode(WINDOWX, WINDOWY), "Looks like a game, maybe ;)");

Player player;
int score = 0;

const int EN = 3;
list<Enemy> enemy(EN);
list<Bullet> bullets;


sf::Vector2i cords = sf::Mouse::getPosition(window);
vector<float> mouse_pos = { (float)cords.x, (float)cords.y };

void make_rotation();
void make_shoot();
void init_Scene() {
    player.shape = sf::CircleShape(player.r);
    player.stem = sf::RectangleShape(sf::Vector2f(player.stem_w, player.stem_h));

    player.shape.setOrigin(player.r, player.r);
    player.stem.setOrigin(player.stem_w/2, 0);
    
    //player.shape.setPosition(player.x, player.y);
    //player.stem.setPosition(player.x + (player.r - player.stem_w) /2, player.y + 2 * (player.r - player.stem_h) - 3);
    player.shape.setFillColor(sf::Color::Yellow);
    player.stem.setFillColor(sf::Color::Yellow);

    for (auto& e: enemy) {
        e.init_enemy(scene, H, offset);
        e.enemy.setSize(sf::Vector2f(e.r, e.r));

        e.enemy.setPosition(e.x, e.y);
        e.enemy.setFillColor(sf::Color::Red);
    }
}

void move_objects(double time) {
    player.move_Player(time, scene, offset);
    for (auto& e : enemy) {
        e.move_Enemy(scene, offset, player);
    }

    for (auto& b : bullets) {
        b.move_Bullet(scene, offset, enemy, WINDOWX, WINDOWY);
    }
    //enemy.move_Enemy(scene, offset, player);

    //system("cls");
    //cout <<offset[0] << enemy.x << "," << enemy.y << endl;
}

void draw_Scene() {
    window.draw(player.shape);
    window.draw(player.stem);

    for (auto& e : enemy) {
        window.draw(e.enemy);
    }

    for (auto& b : bullets) {
        window.draw(b.bullet);
    }


    if(player.x > WINDOWX/2) offset[0] = player.x - WINDOWX/2;
    if(player.y > WINDOWY/2)offset[1] = player.y - WINDOWY/2;

    if (player.x + WINDOWX / 2 > W * 32) offset[0] = W*32 - WINDOWX;
    if (player.y + WINDOWY / 2 > H * 32) offset[1] = H*32 - WINDOWY;

    sf::RectangleShape rect(sf::Vector2f(32, 32));

    for (int i = 0; i < H; i++)
        for (int j = 0; j < W; j++) {
            if (scene[i][j] == 'W') rect.setFillColor(sf::Color::White);

            if (scene[i][j] == ' ') continue;

            rect.setPosition(j * 32 - offset[0], i * 32 - offset[1]);
            window.draw(rect);
        }
}

int main()
{
    
    srand(time(0));
    init_Scene();

    thread new_thrd(make_rotation);
    new_thrd.detach();
       
    while (window.isOpen())
    {
        
        auto now = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> time_diff = now - prev_time;

        
        sf::Event event;
        while (window.pollEvent(event))
        {

            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::LostFocus) {
                player.stepx = 0;
                player.stepy = 0;
               // onPause = true;
            }
            if (event.type == sf::Event::GainedFocus) {
                player.stepx = 0;
                player.stepy = 0;
                onPause = false;
            }

            if (event.type == sf::Event::MouseButtonPressed) {
                //shoot
                cout << "ima bullet" << endl;
                make_shoot();
            }
        }
        //move forward
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            player.stepy = -.2;
            
        //move backward
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            player.stepy = .2;
        
        //move left
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            player.stepx = -.2;
        
        //move right
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            player.stepx = .2;
        
        

        if (player.defeat) {
            //cout << "bump" << endl;
            break;
        }
        
        enemy.remove_if([](Enemy e) {return !e.live; });
        bullets.remove_if([](Bullet b) {return !b.exist; });
        
        if (time_diff.count() >= 28 && !onPause) {
            move_objects(time_diff.count());

            window.clear();
            draw_Scene();
            window.display();

            prev_time = std::chrono::steady_clock::now();
        }
    }

    //show end_game phrase;
    return 0;
}

void make_rotation(){
    while (true) {
        Sleep(20);
        if (cords != sf::Mouse::getPosition()) {
            cords = sf::Mouse::getPosition(window);
            vector<float> player_pos = { (float)player.x + player.r, (float)player.y + player.r };
            mouse_pos = { (float)cords.x + offset[0], (float)cords.y + offset[1] };
            vector<float> ox = { (float)player.x + 1 + player.r, (float)player.y + player.r };
            float new_r = acos(cos(vec(player_pos, mouse_pos),
                vec(player_pos, ox)));
            if (vect_mult(vec(player_pos, mouse_pos), vec(player_pos, ox)) < 0)
                new_r *= -1;
            new_r += M_PI_2;

            player.stem.setRotation(180 * (-new_r) / M_PI);
            player.rotation = new_r;

            system("cls");
            cout << "rotation: " << player.rotation << endl;
        }
    }
}

void make_shoot() {
    bullets.push_back(Bullet());
    bullets.back().init_Bullet(player);
    bullets.back().bullet = sf::CircleShape(bullets.back().r, bullets.back().r);
    bullets.back().bullet.setFillColor(sf::Color::Blue);
    ++score;
}

//void show_stats() {
//    sf::Text result;
//    result.setString("123");
//    result.setCharacterSize(32);
//    result.setPosition(sf::Vector2f(WINDOWX / 2 + offset[0], 0));
//    result.setFillColor(sf::Color::Green);
//    window.draw(result);
//}