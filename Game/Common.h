#pragma once
#include<iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <iterator>
#include <SFML/Graphics.hpp>

using namespace std;

struct vec {
	float x, y;

	vec(vector<float> a, vector<float> b) {
		x = b[0] - a[0];
		y = b[1] - a[1];
	}
};

float vect_mult(vec v1, vec v2);
float cos(vec v1, vec v2);
bool under_line(vec v, vector<float> x0, vector<float> x);
bool above_line(vec v, vector<float> x0, vector<float> x);
