#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"

using namespace std;

class Enemy {
public:
	sf::RectangleShape enemy;
	float x, y, r;
	float stepx, stepy;
	bool live;

	void init_enemy(std::string* TileMap, int H, vector<float> offset);
	void move_Enemy(std::string* TileMap, vector<float> offset, Player& player);
	void collisions(bool axis, std::string* TileMap, Player& player, vector<float> offset);
};

