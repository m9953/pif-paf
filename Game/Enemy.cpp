#include "Enemy.h"
#include <iostream>

void Enemy::init_enemy(std::string* TileMap, int H, vector<float> offset){
    r = 30;
    live = true;
    stepx = 0;
    stepy = 0;

    int i = 0, j = 0;
    do {
        i = rand() % (H-5)+5;
        j = rand() % (TileMap[0].length()-5)+5;
    } while (TileMap[i][j] == 'W');
    x = j * 32 - offset[0];
    y = i * 32 - offset[1];
}

void Enemy::move_Enemy(std::string* TileMap, vector<float> offset, Player& player) {
    stepx = 0;
    stepy = 0;
    
    if (player.x - player.r > x) {
        stepx = 4.8;
    }
    else if (player.x + player.r < x) {
        stepx = -4.8;
    }
    
    if (player.y - player.r > y) {
        stepy = 4.8;
    }
    else if (player.y + player.r < y) {
        stepy = -4.8;
    }
    
    /*stepx = player.x - x;
    stepy = player.y - y;
    
    float tmp = stepx * stepx + stepy * stepy;

    stepx /= tmp;
    stepy /= tmp;*/

    x += stepx;
    collisions(0, TileMap, player, offset);

    y += stepy;
    collisions(1, TileMap, player, offset);

    enemy.setPosition(x - offset[0], y - offset[1]);
}

void Enemy::collisions(bool axis, std::string* TileMap, Player& player, vector<float> offset) {
    for (int i = (y) / 32; i < (y + r) / 32; i++) {
        for (int j = (x ) / 32; j < (x + r) / 32; j++)
        {
            if (TileMap[i][j] == 'W')
            {

                if (axis == 0) {
                    if (stepx > 0) x = j * 32 - r;
                    if (stepx < 0) x = j * 32 + 32;
                }
                if (axis == 1) {
                    if (stepy > 0) y = i * 32 - r;
                    if (stepy < 0) y = i * 32 + 32;
                }
            }
        }
    }

    if ((x <= player.x + player.r && x >= player.x - player.r) ||
        (x + r >= player.x - player.r && x + r <= player.x + player.r)) {

        if ((y <= player.y + player.r && y >= player.y - player.r) ||
            (y + r >= player.y - player.r && y + r <= player.y + player.r)) {
            //cout << "BUMP" << endl;
            player.defeat = true;
        }
    }

}