#include"Common.h"

float vect_mult(vec v1, vec v2) {
	return v1.x * v2.y - v2.x * v1.y;
}

float cos(vec v1, vec v2) {
	return (v1.x * v2.x + v1.y * v2.y) / sqrt(v1.x * v1.x + v1.y * v1.y) / sqrt(v2.x * v2.x + v2.y * v2.y);
}

bool under_line(vec v, vector<float> x0, vector<float> x) {
	return (x[0] - x0[0]) / abs(v.x) > (x[1] - x0[1]) / abs(v.y);
}

bool above_line(vec v, vector<float> x0, vector<float> x) {
	return (x[0] - x0[0]) / abs(v.x) < (x[1] - x0[1]) / abs(v.y);
}