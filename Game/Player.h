#pragma once
#include <SFML/Graphics.hpp>
#include "Common.h"

using namespace std;

class Player {
private:
	int maxx, maxy;
public:
	sf::CircleShape shape;
	sf::RectangleShape stem;
	float x, y, r, stem_h, stem_w;
	float stepx, stepy;
	float rotation;
	bool defeat;

	Player();
	void move_Player(double time, std::string* TileMap, vector<float> offset);
	void collisions(bool axis, std::string* TileMap);
};