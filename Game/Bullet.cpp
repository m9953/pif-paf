#include "Bullet.h"
#include <iostream>

void Bullet::init_Bullet(Player player) {
    exist = true;
    r = 10;

    x = player.x - r;
    y = player.y - r;

    vector<float> tmp{player.y * sin(player.rotation), player.y * cos(player.rotation)};
    float ntmp = tmp[0] * tmp[0] + tmp[1] * tmp[1];
    tmp[0] /= ntmp;
    tmp[1] /= ntmp;

    stepx = tmp[0] * 1000;
    stepy = tmp[1] * 1000;
}

void Bullet::move_Bullet(std::string* TileMap, vector<float> offset, list<Enemy>& enemy, float ww, float wh) {
    x += stepx;
    collisions(0, TileMap, enemy, offset, ww, wh);

    y += stepy;
    collisions(1, TileMap, enemy, offset, ww, wh);

    bullet.setPosition(x - offset[0], y - offset[1]);
}

void Bullet::collisions(bool axis, std::string* TileMap, list<Enemy>& enemy, vector<float> offset, float ww, float wh) {
    for (int i = (y) / 32; i < (y + 2*r) / 32; i++) {
        for (int j = (x) / 32; j < (x + 2*r) / 32; j++)
        {
            if (TileMap[i][j] == 'W')
            {
                exist = false;
                break;
            }
        }
    }

    if (x - offset[0] < 0 || x - offset[0] > ww || y - offset[1] < 0 || y - offset[1] > wh){
        exist = false;
    }

    if (exist) {
        for (auto& e : enemy) {
            if (exist) {
                if (x >= e.x - e.r && x <= e.x + e.r) {
                    if (y >= e.y - e.r && y <= e.y + e.r) {
                        e.live = false;
                        exist = false;
                        cout << "kill" << endl;
                    }
                }
            }
        }
    }

}