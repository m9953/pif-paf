#include "Player.h"

Player::Player(): r(20), stem_h(50), stem_w(10), stepx(0), stepy(0), 
                  maxx(maxx), maxy(maxy), rotation(0), defeat(false), x(100), y(100){}

void Player::move_Player(double time, std::string* TileMap, vector<float> offset) {  

    x += stepx*time;
    collisions(0, TileMap);

    y += stepy*time;
    collisions(1, TileMap);

    shape.setPosition(x - offset[0], y - offset[1]);
    stem.setPosition(x - offset[0], y - offset[1]);

    stepx = 0;
    stepy = 0;
}

void Player::collisions(bool axis, std::string* TileMap) {
    for (int i = (y - r) / 32; i < (y+ r) / 32; i++)
        for (int j = (x - r) / 32; j < (x + r) / 32; j++)
        {
            if (TileMap[i][j] == 'W')
            {

                if (axis == 0) {
                    if (stepx > 0) x = j * 32 - r;
                    if (stepx < 0) x = j * 32 + 32 + r;
                }
                if (axis == 1) {
                    if (stepy > 0) {
                        y = i * 32 - r;
                        stepy = 0;
                    }
                    if (stepy < 0) {
                        y = i * 32 + 32 + r;
                        stepy = 0;
                    }
                }
            }
        }
}