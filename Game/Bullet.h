#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Enemy.h"
#define _USE_MATH_DEFINES 
#include <math.h>

using namespace std;

class Bullet {
public:
	sf::CircleShape bullet;
	float x, y, r;
	float stepx, stepy;
	bool exist;

	void init_Bullet(Player player);
	void move_Bullet(std::string* TileMap, vector<float> offset, list<Enemy>& enemy, float ww, float wh);
	void collisions(bool axis, std::string* TileMap, list<Enemy>& enemy, vector<float> offset, float ww, float wh);
};

